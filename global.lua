entfernungIst = 0
countGPS = 0
GPSOK = false
timerErwuenscht = false
GPSAccuracy = {}

function initializeVar(var, vartype)
    if vartype == "number" then
        if var == nil then
            var = 0
        end
    elseif vartype == "boolean" then
        if var == nil then
            var = false
        end
    elseif vartype == "string" then
        if var == nil then
            var = ""
        end
    elseif vartype == "table" then
        if var == nil then
            var = {}
        end
    end
end

function initializeVariables()
    initializeVar(entfernungIst, "number")
    initializeVar(countGPS, "number")
    initializeVar(timerErwuenscht, "boolean")
    initializeVar(GPSOK, "boolean")
    initializeVar(GPSAccuracy, "table")
end

function msgMitNachricht(msg, fun)
    _Urwigo.MessageBox {
        Text = msg,
        Callback = function(action)
            if action ~= nil then
                fun()
            end
        end
    }
end

function msgMitNachrichtUndBild(msg, med, fun)
    _Urwigo.MessageBox {
        Text = msg,
        Media = med,
        Callback = function(action)
            if action ~= nil then
                fun()
            end
        end
    }
end

function msgMitNachrichtUndButtonsUndBild(msg, Button1, Button2, med, fun1, fun2)
    _Urwigo.MessageBox {
        Text = msg,
        Media = med,
        Buttons = {
            Button1,
            Button2
        },
        Callback = function(action)
            if action ~= nil then
                if (action == "Button1") == true then
                    fun1()
                elseif (action == "Button2") == true then
                    fun2()
                end
            end
        end
    }
end

function msgMitNachrichtUndButtons(msg, textButton1, textButton2, fun1, fun2)
    _Urwigo.MessageBox {
        Text = msg,
        Buttons = {
            textButton1,
            textButton2
        },
        Callback = function(action)
            if action ~= nil then
                if (action == "Button1") == true then
                    fun1()
                elseif (action == "Button2") == true then
                    fun2()
                end
            end
        end
    }
end

function TableLength(table)
    local i = 0
    repeat
        i = i + 1
    until table[i] == nil
    i = i - 1
    return i
end

function moveZone(entfernung, zone, pStart, timerErwuenscht)
    Wherigo.LogMessage("Zone bewegen aufgerufen", Wherigo.LOGINFO)
    local pEnde = Player.ObjectLocation
    local d, Richtung = Wherigo.VectorToPoint(pStart, pEnde)
    local EntfernungReal = d:GetValue 'm'
    local EntfernungZahl = tonumber(d:GetValue 'm')
    entfernungIst = entfernungIst + EntfernungZahl
    local Entfernung1Zahl = entfernung - entfernungIst + 10
    local Entfernung2Zahl = entfernung - entfernungIst
    local Entfernung1Real = Wherigo.Distance(Entfernung1Zahl, 'm')
    local Entfernung2Real = Wherigo.Distance(Entfernung2Zahl, 'm')
    if EntfernungZahl <= 5 and timerErwuenscht == true then
        Aktualisierung:Start()
    else
        if Entfernung1Zahl <= 0 or Entfernung2Zahl <= 0 or entfernungIst >= entfernung then
            Entfernung1Zahl = 15
            Entfernung2Zahl = 5
            Entfernung1Real = Wherigo.Distance(Entfernung1Zahl, 'm')
            Entfernung2Real = Wherigo.Distance(Entfernung2Zahl, 'm')
        end
        local p1 = Wherigo.TranslatePoint(pEnde, Entfernung2Real, (Richtung + 40) % 360)
        local p2 = Wherigo.TranslatePoint(pEnde, Entfernung1Real, Richtung)
        local p3 = Wherigo.TranslatePoint(pEnde, Entfernung2Real, (Richtung - 40) % 360)
        local p4 = Wherigo.TranslatePoint(pEnde, Entfernung2Real, Richtung)
        refreshZone(zone, p1, p2, p3, p4)
        if timerErwuenscht == true then
            Aktualisierung:Start()
        end
    end
end

function setZone(zone, pStart, bearing)
    local p1 = Wherigo.TranslatePoint(pStart, Wherigo.Distance(50, 'm'), bearing)
    local p2 = Wherigo.TranslatePoint(pStart, Wherigo.Distance(40, 'm'), (bearing + 40) % 360)
    local p3 = Wherigo.TranslatePoint(pStart, Wherigo.Distance(40, 'm'), bearing)
    local p4 = Wherigo.TranslatePoint(pStart, Wherigo.Distance(40, 'm'), (bearing - 40) % 360)
    refreshZone(zone, p1, p2, p3, p4)
end

function movePoint(point, distance, bearing)
    return Wherigo.TranslatePoint(point, Wherigo.Distance(distance, 'm'), bearing)
end

function refreshZone(zone, p1, p2, p3, p4)
    zone.Points = {
        p1,
        p2,
        p3,
        p4
    }
    zone.Active = false
    zone.Active = true
end

function variableZuruecksetzen()
    entfernungIst = 0
end

function wideZone(zone, bearing)
    local p1 = Wherigo.TranslatePoint(zone.Points[1], Wherigo.Distance(15, 'm'), (bearing - 45) % 360)
    local p2 = Wherigo.TranslatePoint(zone.Points[2], Wherigo.Distance(15, 'm'), (bearing + 45) % 360)
    local p3 = Wherigo.TranslatePoint(zone.Points[3], Wherigo.Distance(15, 'm'), (bearing + 135) % 360)
    local p4 = Wherigo.TranslatePoint(zone.Points[4], Wherigo.Distance(15, 'm'), (bearing + 225) % 360)
    zone.Points = {
        p1,
        p2,
        p3,
        p4
    }
    zone.Active = false
    zone.Active = true
end

function testGPS()
    Wherigo.LogMessage("GPS Test aufgerufen", Wherigo.LOGINFO)
    countGPS = countGPS + 1
    table.insert(GPSAccuracy, countGPS, Player.PositionAccuracy:GetValue('m'))
    if countGPS == 5 then
        if GPSAccuracy[countGPS - 4] >= 2
                and GPSAccuracy[countGPS - 4] < 15
                and GPSAccuracy[countGPS - 3] >= 2
                and GPSAccuracy[countGPS - 3] < 15
                and GPSAccuracy[countGPS - 2] >= 2
                and GPSAccuracy[countGPS - 2] < 15
                and GPSAccuracy[countGPS - 1] >= 2
                and GPSAccuracy[countGPS - 1] < 15
                and GPSAccuracy[countGPS] >= 2
                and GPSAccuracy[countGPS] < 15 then
            GPSOK = true
            Wherigo.LogMessage("GPS Test erfolgreich", Wherigo.LOGINFO)
        else
            countGPS = 0
        end
    end
    Cd_TestGPS:Start()
end

function Save()
    Wherigo.LogMessage("Speichern", Wherigo.LOGINFO)
    Catridge:RequestSync()
end

function computeSaveCode(stationNumber)
    local name = Player.Name
    local nameSum = 0

    for i = 1, name:len(), 1 do
        nameSum = nameSum + string.byte(name, i) * 5
    end
    return nameSum + stationNumber * 56
end
